---
layout: page
title: About me
permalink: /about
---
## Education
### 2021-2023
#### [University College Lillebælt](https://www.ucl.dk)
Information Technology \
Odense, Denmark\
Grade: 11
### 2019-2021
#### [University of Southern Denmark](https://www.sdu.dk)
Machanical Engineering \
Odense, Denmark
### 2016-2019
#### [HANSENBERG Technical High School](https://www.hansenberg.dk)
Mathematics and Information Technology \
Kolding, Denmark